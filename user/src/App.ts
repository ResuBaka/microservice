import * as fastify from 'fastify';
import * as config from 'config';
import { Server as HTTPServer, IncomingMessage, ServerResponse } from 'http';
import { FastifyInstance, ServerOptions } from 'fastify';
import { AddressInfo } from 'net';
import { IConfig } from 'config';

class App {
  private serverOptions: ServerOptions;
  public server: FastifyInstance<HTTPServer, IncomingMessage, ServerResponse>;
  public config: IConfig;
  public port: number;
  constructor(port?: number, logger?: any) {
    this.serverOptions = {
      // Logger only for production
      logger: !!(process.env.NODE_ENV !== 'development'),
    };

    this.server = fastify(this.serverOptions);
    this.port = config.get('server.port');
    this.decorators();
  }

  async start(): Promise<void> {
    await this.server.listen(this.port);
    this.server.log.info(
      `server listening on ${(<AddressInfo>this.server.server.address()).port}`
    );
  }

  decorators() {
    this.server.decorate('config', config);
  }
}

export default App;
export { App };
